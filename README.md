# Homepage

Hier unsere Webseite: https://openschoolmaps.ch

## Arbeitsblätter

**OSM.org als Kartenviewer:**

* Infos für Lehrpersonen und Aufgaben für Fortgeschrittene: [Infos](https://gitlab.com/openschoolmaps/OpenSchoolMaps.ch/-/jobs/artifacts/master/raw/lehrmittel/osm-org_als_kartenviewer/infos_fuer_lp/osm-org_als_kartenviewer_lp-infos.pdf?job=PDFs)

* Webseite OSM.org kennenlernen: [Arbeitsblatt](https://gitlab.com/openschoolmaps/OpenSchoolMaps.ch/-/jobs/artifacts/master/raw/lehrmittel/osm-org_als_kartenviewer/arbeitsblaetter_fuer_sus/01_webseite_osm-org_kennenlernen.pdf?job=PDFs) | [Lösungen](https://gitlab.com/openschoolmaps/OpenSchoolMaps.ch/-/jobs/artifacts/master/raw/lehrmittel/osm-org_als_kartenviewer/arbeitsblaetter_fuer_sus/01_webseite_osm-org_kennenlernen_solutions.pdf?job=PDFs)

* Webseite OSM.org im Alltag nutzen: [Arbeitsblatt](https://gitlab.com/openschoolmaps/OpenSchoolMaps.ch/-/jobs/artifacts/master/raw/lehrmittel/osm-org_als_kartenviewer/arbeitsblaetter_fuer_sus/02_webseite_osm-org_im_alltag_nutzen.pdf?job=PDFs)

* OpenStreetMap-Daten untersuchen: [Arbeitsblatt](https://gitlab.com/openschoolmaps/OpenSchoolMaps.ch/-/jobs/artifacts/master/raw/lehrmittel/osm-org_als_kartenviewer/arbeitsblaetter_fuer_sus/03_openstreetmap-daten_untersuchen.pdf?job=PDFs)

* Zusatzaufgaben:
[Infos](https://gitlab.com/openschoolmaps/OpenSchoolMaps.ch/-/jobs/artifacts/master/raw/lehrmittel/osm-org_als_kartenviewer/arbeitsblaetter_fuer_sus/04_zusatzaufgaben.pdf?job=PDFs) | [Lösungen](https://gitlab.com/openschoolmaps/OpenSchoolMaps.ch/-/jobs/artifacts/master/raw/lehrmittel/osm-org_als_kartenviewer/arbeitsblaetter_fuer_sus/04_zusatzaufgaben_solutions.pdf?job=PDFs)

**OpenStreetMap bearbeiten:**

* OpenStreetMap bearbeiten: [Arbeitsblatt](https://gitlab.com/openschoolmaps/OpenSchoolMaps.ch/-/jobs/artifacts/master/raw/lehrmittel/osm_bearbeiten/openstreetmap_bearbeiten.pdf?job=PDFs) | [Lösungen](https://gitlab.com/openschoolmaps/OpenSchoolMaps.ch/-/jobs/artifacts/master/raw/lehrmittel/osm_bearbeiten/openstreetmap_bearbeiten_solutions.pdf?job=PDFs)

**uMap der Karteneditor:**

* Mit uMap einen Lageplan erstellen: [Arbeitsblatt](https://gitlab.com/openschoolmaps/OpenSchoolMaps.ch/-/jobs/artifacts/master/raw/lehrmittel/umap/lageplan_erstellen.pdf?job=PDFs)

* Mit uMap eine Story-Map mit Bildern erstellen: [Arbeitsblatt](https://gitlab.com/openschoolmaps/OpenSchoolMaps.ch/-/jobs/artifacts/master/raw/lehrmittel/umap/story-map_erstellen.pdf?job=PDFs)

* Mit uMap eine thematische Online-Karte erstellen: [Arbeitsblatt](https://gitlab.com/openschoolmaps/OpenSchoolMaps.ch/-/jobs/artifacts/master/raw/lehrmittel/umap/online-karte_erstellen.pdf?job=PDFs)

**Einführung in QGIS 3:**

* Infos für Lehrpersonen: [Infos](https://gitlab.com/openschoolmaps/OpenSchoolMaps.ch/-/jobs/artifacts/master/raw/lehrmittel/einfuehrung_in_qgis/infos_fuer_lp/einfuehrung_in_qgis_lp_infos.pdf?job=PDFs)
* Einleitung und Vorbereitung: [Infos](https://gitlab.com/openschoolmaps/OpenSchoolMaps.ch/-/jobs/artifacts/master/raw/lehrmittel/einfuehrung_in_qgis/arbeitsblaetter_fuer_sus/einleitung_und_vorbereitung.pdf?job=PDFs)

1. Was ist ein GIS? [Arbeitsblatt](https://gitlab.com/openschoolmaps/OpenSchoolMaps.ch/-/jobs/artifacts/master/raw/lehrmittel/einfuehrung_in_qgis/arbeitsblaetter_fuer_sus/was_ist_ein_gis.pdf?job=PDFs) | [Lösungen](https://gitlab.com/openschoolmaps/OpenSchoolMaps.ch/-/jobs/artifacts/master/raw/lehrmittel/einfuehrung_in_qgis/arbeitsblaetter_fuer_sus/was_ist_ein_gis_solutions.pdf?job=PDFs)
2. Was sind Geodaten? [Arbeitsblatt](https://gitlab.com/openschoolmaps/OpenSchoolMaps.ch/-/jobs/artifacts/master/raw/lehrmittel/einfuehrung_in_qgis/arbeitsblaetter_fuer_sus/was_sind_geodaten.pdf?job=PDFs) | [Lösungen](https://gitlab.com/openschoolmaps/OpenSchoolMaps.ch/-/jobs/artifacts/master/raw/lehrmittel/einfuehrung_in_qgis/arbeitsblaetter_fuer_sus/was_sind_geodaten_solutions.pdf?job=PDFs)
3. Verwaltung und Erfassung von Geodaten: [Arbeitsblatt](https://gitlab.com/openschoolmaps/OpenSchoolMaps.ch/-/jobs/artifacts/master/raw/lehrmittel/einfuehrung_in_qgis/arbeitsblaetter_fuer_sus/verwaltung_und_erfassung_von_geodaten.pdf?job=PDFs) | [Lösungen](https://gitlab.com/openschoolmaps/OpenSchoolMaps.ch/-/jobs/artifacts/master/raw/lehrmittel/einfuehrung_in_qgis/arbeitsblaetter_fuer_sus/verwaltung_und_erfassung_von_geodaten_solutions.pdf?job=PDFs)
4. Analyse von Geodaten: [Arbeitsblatt](https://gitlab.com/openschoolmaps/OpenSchoolMaps.ch/-/jobs/artifacts/master/raw/lehrmittel/einfuehrung_in_qgis/arbeitsblaetter_fuer_sus/analyse_von_geodaten.pdf?job=PDFs) | [Lösungen](https://gitlab.com/openschoolmaps/OpenSchoolMaps.ch/-/jobs/artifacts/master/raw/lehrmittel/einfuehrung_in_qgis/arbeitsblaetter_fuer_sus/analyse_von_geodaten_solutions.pdf?job=PDFs)
5. Präsentation von Geodaten: [Arbeitsblatt](https://gitlab.com/openschoolmaps/OpenSchoolMaps.ch/-/jobs/artifacts/master/raw/lehrmittel/einfuehrung_in_qgis/arbeitsblaetter_fuer_sus/praesentation_von_geodaten.pdf?job=PDFs) | [Lösungen](https://gitlab.com/openschoolmaps/OpenSchoolMaps.ch/-/jobs/artifacts/master/raw/lehrmittel/einfuehrung_in_qgis/arbeitsblaetter_fuer_sus/praesentation_von_geodaten_solutions.pdf?job=PDFs)

**Zusätzliche Materialien:**

* OpenStreetMap Tagging Cheatsheet: [PDF](https://gitlab.com/openschoolmaps/OpenSchoolMaps.ch/-/jobs/artifacts/master/raw/lehrmittel/OpenStreetMap%20Tagging%20Cheatsheet.pdf?job=PDFs) | [Word-Dokument](https://gitlab.com/openschoolmaps/OpenSchoolMaps.ch/-/jobs/artifacts/master/raw/lehrmittel/OpenStreetMap%20Tagging%20Cheatsheet.docx?job=PDFs)
* Einführung in QGIS 3: [Daten](https://gitlab.com/openschoolmaps/OpenSchoolMaps.ch/-/jobs/artifacts/master/download?job=QGIS%20excercise%20data)

Diese Arbeitsblätter (PDFs) werden
aus den Dateien
auf [dieser Seite](https://gitlab.com/openschoolmaps/OpenSchoolMaps.ch/tree/master/lehrmittel), die in der Auszeichnungssprache [AsciiDoc](https://asciidoctor.org/docs/what-is-asciidoc/) geschrieben sind,
erzeugt.

Wenn Ihnen Fehler auffallen oder etwas einfällt, was man an den Unterrichtsmaterialien verbessern kann, schauen Sie sich die Seite "Weitere Unterrichtsideen" an.

### English version

Get [these materials in English](https://gitlab.com/openschoolmaps/OpenSchoolMaps.ch/-/jobs/artifacts/english/download?job=PDFs) (Zip archive).
