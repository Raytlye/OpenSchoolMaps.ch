= OSM.org als Karten-Viewer, Blatt 2: Webseite OSM.org im Alltag benutzen
OpenSchoolMaps.ch -- Freie Lernmaterialien zu freien Geodaten und Karten
:imagesdir: ../../../bilder/
include::../../../snippets/lang/de.adoc[]
include::../../../snippets/suppress_title_page.adoc[]

*Ein Arbeitsblatt*

NOTE: In diesem Arbeitsblatt lernst du, den Kartenstil auf openstreetmap.org zu wechseln, die Routenplanung zu verwenden und auf der Karte gefundene Positionen anderen mitzuteilen.

== Kartenstile

Trotz des Namens und der prominenten Ansicht auf openstreetmap.org ist OpenStreetMap eigentlich gar keine Karte. Es ist eine detaillierte geographische Datensammlung, aus der Computer eine Karte erzeugen können. Und nicht nur eine Karte, sondern -- basierend auf den gleichen Daten -- verschieden Karten für verschiedene Zwecke.

Einige davon bietet die openstreetmap.org-Website zur Auswahl an. Was du bisher gesehen hast, ist der OpenStreetMap-Standard-Kartenstil. Wenn du auf das kleine Stapel-Symbol image:osm_editieren/osm_ebene_icon.PNG["Flächen-Icon", 20, 20] über dem image:osm-org_als_kartenviewer/info_button.PNG["Info Button", 20 , 20]-Knopf auf der rechten Seite klickst, kannst du zwischen diesen verschiedenen Kartendarstellungen wechseln.

== Navigation (Routen-Planung)

Mit der OpenStreetMap-Geodatensammlung können Computer mehr machen, als "nur" Karten zu zeichnen! Die Such-Funktion, die wir bereits angesehen haben, verwendet dieselben Daten und mischt zur Vervollständigung noch zusätzliche freie Daten, z.B. aus WikiData (einem Schwester-Projekt von Wikipedia) hinzu.

Auch Wege von A nach B zu finden, ist mit den OpenStreetMap-Daten möglich. Das geht so:

Klicke auf den image:osm-org_als_kartenviewer/navigation_button.PNG["Navigation Button", 25, 25]-Knopf neben dem Suchfeld. Nun kannst du entweder per Text-Eingabe (z.B. Adresse) nach Start- und Zielort suchen, oder die rote und grüne Stecknadel aus dem Suchformular auf die entsprechende Stelle auf der Karte ziehen.

Mit dem Dropdown-Feld unterhalb der Start- und Ziel-Angabe kannst du das Verkehrsmittel/die Bewegungsart (per Auto, per Velo oder zu Fuss) und das Weg-Such-Programm (in der Fachsprache: den Routing-Algorithmus) wählen.

.Zwischenstationen
[NOTE]
====
Die Angabe von Zwischenstopps oder "Via"s ist auf openstreetmap.org leider nicht möglich. Unter http://routing.osm.ch gibt es eine weitere OpenStreetMap-basierte Routen-Planungs-Möglichkeit, die jedoch nur für Strecken innerhalb der Schweiz funktioniert.
====

=== Aufgaben

Untersuche mit der Navigations-Funktion deinen Schulweg.

Wie lange ist er mit dem von dir normalerweise verwendeten Verkehrsmittel?

////
(ÖV/Fahrplan-Abfrage wird leider nicht unterstützt.)

Angabe in Metern oder Kilometern
////

====
{empty} +
====


Stimmt die vorausgesagte Dauer mit deiner Erfahrung überein?

====
{empty} +
====


Stimmt die vorgeschlagene Strecke mit der überein, die du normalerweise nimmst?

====
{empty} +
====


Wird für jedes Verkehrsmittel die selbe Strecke vorgeschlagen?

====
{empty} +
====


Wieso kann wohl je nach Verkehrsmittel eine andere Strecke vorgeschlagen werden?

////
Nicht jedes Verkehrsmittel ist überall erlaubt:
Mit dem Auto darf man nicht durch die Fussgängerzone,
zu Fuss nicht über die Autobahn.

Auch welche Strecke die schnellste ist,
kann von der gewählten Fortbewegungsart abhängen:
Im Auto kann sich ein kleiner Umweg lohnen,
um eine 30er-Zone zu vermeiden. Velofahrerinnen
und Fussgänger werden von einer solchen nicht
ausgebremst.
////

====
{empty} +
====

== Positionen (mit)teilen

Wenn du jemandem einen Link zu einem Karten-Ausschnitt oder einer markierten Stelle auf der Karte schicken willst, erhältst du einen solchen wenn du auf den image:osm-org_als_kartenviewer/share_button.PNG["Share Button", 20, 20]-Knopf rechts klickst.

include::../../../snippets/quellenangabe.adoc[]
